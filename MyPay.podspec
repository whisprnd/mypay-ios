#
# Be sure to run `pod lib lint MyPay.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

require 'json'

# Returns the version number for a package.json file
pkg_version = lambda do |dir_from_root = '', version = 'version'|
  path = File.join(__dir__, dir_from_root, 'package.json')
  JSON.parse(File.read(path))[version]
end

# Let the main package.json decide the version number for the pod
MyPay_version = pkg_version.call
# Use the same RN version that the JS tools use
react_native_version = pkg_version.call('node_modules/react-native')

Pod::Spec.new do |s|
  s.name             = 'MyPay'
  s.version          = MyPay_version
  s.description      = 'MyPay mobile payments'
  s.summary          = 'MyPay iOS SDK'
  s.homepage         = 'https://www.whisp.software'
  s.license          = { type: 'MIT', file: 'LICENSE' }
  s.author           = { 'Anton Zagrebelny' => 'anton@whisp.software' }
  s.source           = { git: 'https://bitbucket.org/whisprnd/mypay-ios.git', tag: s.version.to_s }

  s.source_files   = 'Pod/Classes/**/*.{h,m}'
  s.resources      = 'Pod/Assets/**/*.{js,storyboard,xib,ttf}'
  s.platform       = :ios, '9.0'

  # React is split into a set of subspecs, these are the essentials
  s.dependency 'React/Core', react_native_version
  s.dependency 'React/CxxBridge', react_native_version
  s.dependency 'React/RCTAnimation', react_native_version
  s.dependency 'React/RCTImage', react_native_version
  s.dependency 'React/RCTLinkingIOS', react_native_version
  s.dependency 'React/RCTNetwork', react_native_version
  s.dependency 'React/RCTText', react_native_version
  s.dependency 'React/ART', react_native_version

  # React's dependencies
  s.dependency 'yoga', "#{react_native_version}.React"

  podspecs = [
    'node_modules/react-native/third-party-podspecs/DoubleConversion.podspec',
    'node_modules/react-native/third-party-podspecs/Folly.podspec',
    'node_modules/react-native/third-party-podspecs/glog.podspec',
    'node_modules/react-native-gesture-handler/RNGestureHandler.podspec',
    'node_modules/react-native-svg/RNSVG.podspec'
  ]
  podspecs.each do |podspec_path|
    spec = Pod::Specification.from_file podspec_path
    s.dependency spec.name, "#{spec.version}"
  end
end
