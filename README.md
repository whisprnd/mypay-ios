# MyPay

[![CI Status](https://img.shields.io/travis/Anton Zagrebelny/MyPay.svg?style=flat)](https://travis-ci.org/Anton Zagrebelny/MyPay)
[![Version](https://img.shields.io/cocoapods/v/MyPay.svg?style=flat)](https://cocoapods.org/pods/MyPay)
[![License](https://img.shields.io/cocoapods/l/MyPay.svg?style=flat)](https://cocoapods.org/pods/MyPay)
[![Platform](https://img.shields.io/cocoapods/p/MyPay.svg?style=flat)](https://cocoapods.org/pods/MyPay)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MyPay is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MyPay'
```

## Author

Anton Zagrebelny, anton@whisp.software

## License

MyPay is available under the MIT license. See the LICENSE file for more info.
