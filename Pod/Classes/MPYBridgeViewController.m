#import "MPYBridgeViewController.h"
#import <React/RCTRootView.h>
#import <React/RCTBridgeDelegate.h>
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>

@interface MPYBridgeViewController () <RCTBridgeDelegate>
@end

@implementation MPYBridgeViewController

- (void)loadView
{
    RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:@{}];
    self.view = [[RCTRootView alloc] initWithBridge:bridge
                                         moduleName:@"MyPay"
                                  initialProperties:self.initialProperties];
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
    BOOL isLocalDevelopment = (BOOL) [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"MPYLocalDevelopment"] boolValue];
    
    if (isLocalDevelopment) {
        NSURL *jsUrl = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
        [[RCTBundleURLProvider sharedSettings] setJsLocation:jsUrl.host];
        return jsUrl;
    }
    else {
        return [[NSBundle bundleForClass:self.class] URLForResource:@"MyPay" withExtension:@"js"];
    }
}

@end
