#import "MyPay.h"
#import "MPYBridgeViewController.h"
#import <CoreText/CoreText.h>

void CFSafeRelease(CFTypeRef cf) {
    if (cf != NULL) {
        CFRelease(cf);
    }
}

@interface MyPay ()
@property (nonatomic, strong) MPYBridgeViewController* _bridgeVC;
@property (nonatomic, strong) NSString* _partnerApiToken;
@end

@implementation MyPay

+ (instancetype) sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadFonts];
    }
    return self;
}

- (void) initialize:(NSString *)partnerApiToken
{
    self._partnerApiToken = partnerApiToken;
}

- (void) pay: (NSString*) memberId
{
    UIViewController* presenter = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MyPay" bundle:[NSBundle bundleForClass:self.class]];
    
    self._bridgeVC = (MPYBridgeViewController*) [storyboard instantiateInitialViewController];
    self._bridgeVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    NSMutableDictionary *initialProps = [[NSMutableDictionary alloc] init];
    [initialProps setObject:self._partnerApiToken forKey:@"partnerApiToken"];
    if (memberId){
        [initialProps setObject:memberId forKey:@"memberId"];
    }
    
    self._bridgeVC.initialProperties = initialProps;
    
    [presenter presentViewController:self._bridgeVC animated:true completion:nil];
}

- (void) dismiss
{
    if (self._bridgeVC) {
        [self._bridgeVC dismissViewControllerAnimated:true completion:nil];
        self._bridgeVC = nil;
    }
}

- (void) loadFonts {
    NSArray* fonts = [NSArray arrayWithObjects:
                      @"Open Sans Hebrew",
                      @"Open Sans Hebrew Bold",
                      @"Rubik-Black",
                      @"Rubik-BlackItalic",
                      @"Rubik-Bold",
                      @"Rubik-BoldItalic",
                      @"Rubik-Italic",
                      @"Rubik-Light",
                      @"Rubik-LightItalic",
                      @"Rubik-Medium",
                      @"Rubik-MediumItalic",
                      @"Rubik-Regular",
                      @"rubicon-icon-font",
                      nil];
    
    NSBundle*bundle = [NSBundle bundleForClass:self.class];
    
    for (NSString * font in fonts) {
        NSURL *fontURL = [bundle URLForResource:font withExtension:@"ttf"];
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)[NSData dataWithContentsOfURL:fontURL]);
        CGFontRef fontRef = CGFontCreateWithDataProvider(provider);
        
        CFErrorRef error;
        if (!CTFontManagerRegisterGraphicsFont(fontRef, &error)) {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            NSLog(@"[MyPay] Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        
        CFSafeRelease(fontRef);
        CFSafeRelease(provider);
    }
}

@end
