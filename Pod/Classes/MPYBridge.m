#import "MPYBridge.h"

@implementation MPYBridge

RCT_EXPORT_MODULE(MyPay);

RCT_EXPORT_METHOD(dismiss)
{
    [[MyPay sharedInstance] dismiss];
}

@end
