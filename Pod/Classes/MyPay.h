#import <Foundation/Foundation.h>

@interface MyPay : NSObject
+ (instancetype _Nonnull) sharedInstance;
- (void) initialize: (NSString* _Nonnull) partnerApiToken;
- (void) pay: (NSString* _Nullable) memberId;
- (void) dismiss;
@end
