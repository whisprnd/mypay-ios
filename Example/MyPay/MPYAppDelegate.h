//
//  MPAppDelegate.h
//  MyPay
//
//  Created by Anton Zagrebelny on 05/07/2019.
//  Copyright (c) 2019 Anton Zagrebelny. All rights reserved.
//

@import UIKit;

@interface MPYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
