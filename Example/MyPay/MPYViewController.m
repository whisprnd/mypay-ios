//
//  MPViewController.m
//  MyPay
//
//  Created by Anton Zagrebelny on 05/07/2019.
//  Copyright (c) 2019 Anton Zagrebelny. All rights reserved.
//

#import "MPYViewController.h"
#import "MyPay.h"

@interface MPYViewController ()
- (IBAction)payButtonPressed:(id)sender;
@end

@implementation MPYViewController

- (IBAction)payButtonPressed:(id)sender {
    [[MyPay sharedInstance] pay:@"demo-member-id"];
}

@end
