//
//  main.m
//  MyPay
//
//  Created by Anton Zagrebelny on 05/07/2019.
//  Copyright (c) 2019 Anton Zagrebelny. All rights reserved.
//

@import UIKit;
#import "MPYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPYAppDelegate class]));
    }
}
